using Test

function testa_matrix_pot()
    @test matrix_pot([1 2 ; 3 4], 1) == [1 2 ; 3 4] ^ 1
    @test matrix_pot([1 2 ; 3 4], 2) == [1 2 ; 3 4] ^ 2
    @test matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7) == [4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7] ^ 7
    @test matrix_pot([1 2 1 ; 0 1 1 ; -1 3 4], 7) == [1 2 1 ; 0 1 1 ; -1 3 4] ^ 7
    println("Fim dos testes")
end

function testa_matrix_pot_by_squaring()
    @test matrix_pot_by_squaring([1 2 ; 3 4], 1) == [1 2 ; 3 4] ^ 1
    @test matrix_pot_by_squaring([1 2 ; 3 4], 2) == [1 2 ; 3 4] ^ 2
    @test matrix_pot_by_squaring([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7) == [4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7] ^ 7
    @test matrix_pot_by_squaring([1 2 1 ; 0 1 1 ; -1 3 4], 7) == [1 2 1 ; 0 1 1 ; -1 3 4] ^ 7
    println("Fim dos testes")
end

using LinearAlgebra

function compare_times()
    print("Para efetuar [4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7] ^ 10, a função matrix_pot levou")
    @time matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 10)
    print("Para efetuar [4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7] ^ 10, a função matrix_pot_by_squaring levou")
    @time matrix_pot_by_squaring([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 10)
    print("Para efetuar [1 2 1 ; 0 1 1 ; -1 3 4] ^ 10, a função matrix_pot levou")
    @time matrix_pot([1 2 1 ; 0 1 1 ; -1 3 4], 10)
    print("Para efetuar [1 2 1 ; 0 1 1 ; -1 3 4] ^ 10, a função matrix_pot_by_squaring levou")
    @time matrix_pot_by_squaring([1 2 1 ; 0 1 1 ; -1 3 4], 10)
end

function multiplica(a, b)
    dima = size(a)
    dimb = size(b)
    if dima[2] != dimb[1]
        return -1
    end
    c = zeros(dima[1], dimb[2])
    for i in 1:dima[1]
        for j in 1:dimb[2]
            for k in 1:dima[2]
                c[i, j] = c[i, j] + a[i, k] * b[k, j]
            end
        end
    end
    return c
end

function matrix_pot(M, p)
    N = M
    for i in 2:p
        N = multiplica(N, M)
    end
    return N
end

function matrix_pot_by_squaring(M, p)
    if p == 1
        return M
    else
        N = matrix_pot_by_squaring(M, div(p, 2))
        N = multiplica(N, N)
        if p % 2 == 0
            return N
        else
            return multiplica(N, M)
        end
    end
end

compare_times()
